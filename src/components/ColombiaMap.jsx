import React, { useState, useEffect } from 'react';
import { geoMercator, geoEqualEarth, geoPath } from 'd3-geo';
import { feature } from 'topojson-client';
// create import for .json data on assets folder
import geojson from '../assets/colombia.json';
import { select } from 'd3-selection';

const ColombiaMap = () => {
  const [geographies, setGeographies] = useState([]);
  const width = 1920;
  const height = width * 0.5;
  const projection = geoMercator().fitExtent(
    [
      [0, 0],
      [width * 0.9, height * 0.9],
    ],
    geojson
  );
  const path = geoPath().projection(projection);

  // create function to load json data from file on assets folder
  const loadJsonData = async () => {
    const data = await geojson;
    setGeographies(data);
  };

  useEffect(() => {
    loadJsonData();
  }, []);

  return (
    <div>
      <svg width={width} height={height}>
        <g className='geojson-layer'>
          {geojson.features.map((d) => {
            return (
              <path
                key={d.properties.Name}
                d={path(d)}
                fill='#ccc'
                stroke='gray'
                strokeWidth='1'
                strokeOpacity='0.5'
                onMouseEnter={(e) => {
                  select(e.target).attr('fill', '#0597ff');
                }}
                onMouseOut={(e) => {
                  select(e.target).attr('fill', '#ccc');
                }}
              />
            );
          })}
        </g>
      </svg>
    </div>
  );
};

export default ColombiaMap;
