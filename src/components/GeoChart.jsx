import React, { useState, useEffect } from 'react';
import { feature } from 'topojson-client';
// create import for .json data on assets folder
import geojson from '../assets/colombia.json';
import {
  select,
  geoPath,
  geoMercator,
  min,
  max,
  scaleLinear,
  geoEqualEarth,
} from 'd3';

// create function to extract key from json data
const getKeys = (json) => {
  const keys = [];
  json.features.forEach((feature) => {
    keys.push({
      key: feature.properties.DPTO,
      name: feature.properties.NOMBRE_DPT,
    });
  });
  return keys;
};

const GeoChart = ({ data, property }) => {
  const [geographies, setGeographies] = useState([]);
  const [tooltipContent, setTooltipContent] = useState({});
  const startColor = '#38E54D'; // Rojo
  const endColor = '#C21010'; // Verde
  const width = 1000;
  const height = width * 0.5;
  const projection = geoMercator().fitExtent(
    [
      [0, 0],
      [width * 0.9, height * 0.9],
    ],
    geojson
  );

  const minProp = min(data, (d) => d[property]);
  const maxProp = max(data, (d) => d[property]);
  const colorScale = scaleLinear()
    .domain([minProp, maxProp])
    .range([startColor, endColor]);

  const path = geoPath().projection(projection);

  const getColor = (key) => {
    const value = data.find((d) => d.key === key);
    if (value) {
      return colorScale(value.value);
    }
  };

  const loadGeoData = async () => {
    setGeographies(data);
  };

  useEffect(() => {
    loadGeoData();
  }, []);

  const tooltip = select('body')
    .append('div')
    .attr('id', 'tooltip')
    .style('opacity', 0)
    .style('position', 'absolute')
    .style('background-color', '#fff')
    .style('border-radius', '5px')
    .style('padding', '10px')
    .style('box-shadow', '0 0 10px #000')
    .style('pointer-events', 'none')
    .style('font-size', '12px')
    .style('font-family', 'Arial')
    .style('white-space', 'pre-line')

  const getTooltipContent = (geo) => {
    const value = data.find((d) => d.key === geo.properties.DPTO);
    if (value) {
      return `DPTO: ${geo.properties.DPTO}\n${value.name}: ${value.value}
      `;
    }
    return 'No data';
  };

  return (
    <div className='body'>
      <svg width={width} height={height}>
        <g className='geojson-layer'>
          {geojson.features.map((d) => {
            return (
              <path
                key={d.properties.DPTO}
                d={path(d)}
                fill={getColor(d.properties.DPTO)}
                stroke='gray'
                strokeWidth='1'
                strokeOpacity='0.5'
                onMouseEnter={(e) => {
                  select(e.target).attr('fill', 'gray');
                  select('#tooltip')
                    .style('opacity', 1)
                    .text(getTooltipContent(d));
                }}
                onMouseOut={(e) => {
                  select(e.target).attr('fill', getColor(d.properties.DPTO));
                  select('#tooltip').style('opacity', 0);
                }}
                onMouseOver={(e) => {}}
                onMouseMove={(e) => {
                  console.log(
                    '🚀 ~ file: GeoChart.jsx:1 ~ {geojson.features.map ~ e:',
                    e
                  );

                  select('#tooltip')
                    .style('left', e.pageX + 10 + 'px')
                    .style('top', e.pageY + 10 + 'px');
                }}
              />
            );
          })}
        </g>
      </svg>
    </div>
  );
};

export default GeoChart;
