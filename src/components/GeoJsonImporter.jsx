import React from 'react';

class GeoJSONImporter extends React.Component {
  handleFileUpload = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      const content = e.target.result;
      const geoJSONData = JSON.parse(content);
      // Do something with the GeoJSON data
      console.log(geoJSONData);
    };

    reader.readAsText(file);
  };

  render() {
    return (
      <div>
        <input type="file" accept=".geojson" onChange={this.handleFileUpload} />
      </div>
    );
  }
}

export default GeoJSONImporter;