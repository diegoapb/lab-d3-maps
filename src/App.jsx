import { useState } from 'react';
import reactLogo from './assets/react.svg';
import viteLogo from '/vite.svg';
import './App.css';
import GeoChart from './components/GeoChart';
import GeoJSONImporter from './components/GeoJsonImporter';
import demoData from './assets/demo_data_counter_numeric.json';

function App() {

  return (
    <>
      <GeoChart data={demoData} property={'value'} />
    </>
  );
}

export default App;
